defmodule Reactive.LevelDbIndexDao do
  require Logger

  def get_or_create_index(index_name) do
    [{:leveldb, db}] = :ets.lookup(ReactiveLevelDb, :leveldb)
    :eleveldb.put(db,"ei:" <> index_name <> ":a","entity index begin",[])
    :eleveldb.put(db,"ei:" <> index_name <> ":z","entity index end",[])
    index_name
  end

  def add(index_name, keys, id) when is_list(keys) do
    [{:leveldb, db}] = :ets.lookup(ReactiveLevelDb, :leveldb)
    db_id = Reactive.EntitiesDb.entity_db_id(id)
    bin_id = :erlang.term_to_binary(id)
    prefix = "ei:" <> index_name <> ":i:"
    for(key <- keys) do
      keyPrefix = prefix <> key <> ":"
      :eleveldb.put(db, keyPrefix <> db_id,bin_id,[])
    end
  end

  def add(index_name, key, id) when is_binary(key) do
    [{:leveldb, db}] = :ets.lookup(ReactiveLevelDb, :leveldb)
    db_id = Reactive.EntitiesDb.entity_db_id(id)
    keyPrefix = "ei:" <> index_name <> ":i:" <> key <> ":"
    :eleveldb.put(db, keyPrefix <> db_id,:erlang.term_to_binary(id),[])
  end

  def remove(index_name, keys, id) when is_list(keys) do
    [{:leveldb, db}] = :ets.lookup(ReactiveLevelDb, :leveldb)
    db_id = Reactive.EntitiesDb.entity_db_id(id)
    bin_id = :erlang.term_to_binary(id)
    prefix = "ei:" <> index_name <> ":i:"
    for(key <- keys) do
      keyPrefix = prefix <> key <> ":"
      :eleveldb.delete(db, keyPrefix <> db_id,bin_id,[])
    end
  end

  def remove(index_name, key, id) when is_binary(key) do
    [{:leveldb, db}] = :ets.lookup(ReactiveLevelDb, :leveldb)
    db_id = Reactive.EntitiesDb.entity_db_id(id)
    bin_id = :erlang.term_to_binary(id)
    prefix = "ei:" <> index_name <> ":i:"
    keyPrefix = prefix <> key <> ":"
    :eleveldb.delete(db, keyPrefix <> db_id,bin_id,[])
  end

  def update(index_name, old_keys, new_keys, id) when is_list(old_keys) and is_list(new_keys) do
    o = MapSet.new(old_keys)
    n = MapSet.new(new_keys)
    r = MapSet.difference(o,n)
    a = MapSet.difference(n,o)
    remove(index_name, MapSet.to_list(r), id)
    add(index_name, MapSet.to_list(a), id)
  end

  def update(index_name, old_key, new_key, id) when is_binary(old_key) and is_binary(new_key) do
    remove(index_name, old_key, id)
    add(index_name, new_key, id)
  end

  def find(index_name, key) do
    [{:leveldb, db}] = :ets.lookup(ReactiveLevelDb, :leveldb)
    keyPrefix = "ei:" <> index_name <> ":i:" <> key <> ":"
    values = Reactive.Db.scan(db,%{
      prefix: keyPrefix,
      fetch: :value
    })
    results = Enum.map(values,fn(b) -> :erlang.binary_to_term(b) end)
    Logger.debug("Index #{index_name} FIND #{key} -> #{inspect results}")
    results
  end
end