defmodule Reactive.LevelDbLogDao do

  def init(log) do
    [{:leveldb, db}] = :ets.lookup(ReactiveLevelDb, :leveldb)
    :eleveldb.put(db,Reactive.LevelDbEntityId.entity_db_id(log) <> ":a","log begin",[])
    :eleveldb.put(db,Reactive.LevelDbEntityId.entity_db_id(log) <> ":z","log end",[])
    log
  end

  def open(id) do
    id
  end

  def push(log,key,data) do
    [{:leveldb,db}] = :ets.lookup(ReactiveLevelDb, :leveldb)
    :eleveldb.put(db,Reactive.LevelDbEntityId.entity_db_id(log) <> ":l:" <> key,:erlang.term_to_binary(data),[])
  end

  def remove(log,key) do
    [{:leveldb,db}] = :ets.lookup(ReactiveLevelDb, :leveldb)
    :eleveldb.delete(db,Reactive.LevelDbEntityId.entity_db_id(log) <> ":m:" <> key,[])
  end

  def overwrite(log,key,data) do
    [{:leveldb,db}] = :ets.lookup(ReactiveLevelDb, :leveldb)
    :eleveldb.put(db,Reactive.LevelDbEntityId.entity_db_id(log) <> ":l:" <> key,:erlang.term_to_binary(data),[])
  end

  def fetch(log,from,to,limit,reverse) do
    [{:leveldb,db}] = :ets.lookup(ReactiveLevelDb, :leveldb)
    log_id = Reactive.LevelDbEntityId.entity_db_id(log)
    {start,climit} = case from do
                       :begin -> {"a",limit+1}
                       :end -> {"z",limit+1}
                       key -> {"l:" <> key,limit}
                     end
    scr=Reactive.LevelDbScan.scan(db,%{
      :prefix => log_id <> ":",
      :begin => start,
      :end => case to do
        :begin -> "a"
        :end -> "z"
        key -> "l:" <> key
      end,
      :limit => climit,
      :reverse => reverse
    })
    #Logger.info("scanr=#{inspect scr}")
    lprefix = "l:"
    lprefix_size = byte_size(lprefix)
    lsr=:lists.filtermap(fn({fullKey,value}) ->
      case fullKey do
        << ^lprefix :: binary-size(lprefix_size), key :: binary >> -> {true,{key,:erlang.binary_to_term(value)}}
        _ -> false
      end
    end,scr)
   # Logger.info("log scanr=#{inspect lsr}")
    lsr
  end
  
end