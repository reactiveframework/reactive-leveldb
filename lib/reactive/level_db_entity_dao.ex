defmodule Reactive.LevelDbEntityDao do

  def load(id) do
    [{:leveldb,db}] = :ets.lookup(ReactiveLevelDb, :leveldb)
    db_id = Reactive.LevelDbEntityId.entity_db_id(id)
    case Reactive.Db.get(db,db_id) do
      {:ok, binary} -> {:ok,:erlang.binary_to_term(binary)}
      not_found -> not_found
    end
  end

  def store(id,data) do
    [{:leveldb,db}] = :ets.lookup(ReactiveLevelDb, :leveldb)
    db_id = Reactive.LevelDbEntityId.entity_db_id(id)
    :eleveldb.put(db,db_id,:erlang.term_to_binary(data),[])
    :ok
  end

end