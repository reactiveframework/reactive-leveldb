defmodule ReactiveLevelDb do
  use Application
  require Logger

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      # Define workers and child supervisors to be supervised
      # worker(ReactiveLeveldb.Worker, [arg1, arg2, arg3]),
    ]

    path = Application.get_env(:reactive_leveldb,:path,"db.leveldb")
    {:ok, leveldb} = :eleveldb.open(to_char_list(path), [create_if_missing: true])
    Logger.debug("LEVELDB: #{inspect leveldb} IN #{inspect path}")

    :ets.new(ReactiveLevelDb, [:named_table, :public, :set, {:keypos, 1}])
    :ets.insert(ReactiveLevelDb, {:leveldb, leveldb})

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ReactiveLeveldb.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
