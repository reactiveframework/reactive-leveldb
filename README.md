# ReactiveLeveldb

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add reactive_leveldb to your list of dependencies in `mix.exs`:

        def deps do
          [{:reactive_leveldb, "~> 0.0.1"}]
        end

  2. Ensure reactive_leveldb is started before your application:

        def application do
          [applications: [:reactive_leveldb]]
        end

